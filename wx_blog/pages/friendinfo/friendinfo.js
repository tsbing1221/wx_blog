//获取应用实例
const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;
Page({
  data: {
		userInfo: {},
    isShowModal: true,
		isFriend: false
  },
  cancel: function () {
    this.setData({
      isShowModal: !this.data.isShowModal
    });
  },
  commentInput: function (e) {
    this.setData({
      user_comment: e.detail.value
    });
  },
  showModal: function() {
    this.setData({
      isShowModal: false
    });
  },
  confirm: function () {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/addUserApply',
      method: "post",
      data: {
        myphone: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid,
        openid: that.data.friendInfo.openid,
				comment: that.data.user_comment
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            isShowModal: !that.data.isShowModal,
            user_comment: ''
          });
          $Toast({
            content: res.data.payload,
            type: 'success'
          });
        } else {
          that.setData({
            isShowModal: !that.data.isShowModal,
            user_comment: ''
          });
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  },
  onLoad: function(options) {
    var that = this;

		var userInfo = wx.getStorageSync("loginInfo");
		var friendInfo = wx.getStorageSync("friendInfo");
		that.setData({ userInfo: userInfo, friendInfo: friendInfo});

		that.checkIsFriend();
  },
  onPullDownRefresh: function() {

  },

  onReachBottom: function() {

  },

  onShareAppMessage: function(res) {
    return {
      title: '空城丶博客',
      path: '/pages/friend/index'
    }
  },

	chat: function() {
		var chatInfo = {
			userInfo: {
				account: this.data.userInfo.account,
				app_sid: this.data.userInfo.app_sid,
				avatar: this.data.userInfo.avatar,
				username: this.data.userInfo.username
			},
			friendInfo: {
				account: this.data.friendInfo.account,
				app_sid: this.data.userInfo.app_sid,
				avatar: this.data.friendInfo.avatar,
				username: this.data.friendInfo.username
			}
		}

		this.data.friendInfo.rename ? chatInfo.friendInfo.rename = this.data.friendInfo.rename : '';

		wx.setStorage({
			key: 'chatInfo',
			data: chatInfo,
		});

		//跳转到聊天界面
		wx.navigateTo({
			url: '/pages/chat/chat',
		})
	},

  details(e) {
    //详情页跳转
    wx.navigateTo({
      url: '../details/details?id=' + e.currentTarget.id
    })
  },
	checkIsFriend: function() {
    var that = this;

		console.log({
			myphone: that.data.userInfo.account,
			friendphone: that.data.friendInfo.account
		})

    wx.request({
			url: util.basePath + '/article/v1/checkIsFriend',
      method: "post",
      data: {
        myphone: that.data.userInfo.account,
        friendphone: that.data.friendInfo.account
      },
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
						isFriend: true
          });
        } else {
          console.log(res.data.err);
        }
      }
    });
  }
});