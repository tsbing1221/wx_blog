const app = getApp();
const util = require('../../utils/util.js');
const { $Toast } = require('../../dist/base/index');
var pageNo = 1;

Page({
  data: {
    current: 0, //当前所在滑块的 index
    scrollLeft: 0, //滚动条的位置,一个选项卡宽度是90（自定义来自css），按比例90*n设置位置
    navlist: ["全部", "NodeJS", "Nginx", "前端", "数据库"],
    imgUrls: [
      "http://pic.niyueling.cn/upload_avatar/2019/10/31/0/0/134254983.png",
      'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1906254523,3408854892&fm=26&gp=0.jpg',
      "http://pic.niyueling.cn/upload_avatar/2019/10/31/0/0/134331995.png"
    ],
    content: [],
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 5000, //自动切换时间间隔
    duration: 1000, //滑动动画时长
    tip: "",
    loading: false
  },

  //查看公告和使用规则
  getNotice: function(e) {
    var index = e.target.dataset.index;

    if (index == 0) {
      wx.navigateTo({
        url: '/pages/details/details?id=21869',
      });
    }

    if (index == 2) {
      wx.navigateTo({
        url: '/pages/details/details?id=21893',
      });
    }
  },

  //tab切换
  tab: function(event) {
    var cateUrl = this.data.navlist[event.target.dataset.current].labels;
		var that = this;
    that.setData({
      current: event.target.dataset.current
    });
    wx.navigateTo({
      url: '../list/list?cateUrl=' + cateUrl
    });
  },

  onSearch: function(event) {
    wx.navigateTo({
      url: '../search/search?keyword=' + event.detail.value
    });
  },

  onLoad: function() {
    //加载  
    var that = this;
    this.getIndexList();
    this.getCategories();
  },
  
  onPullDownRefresh: function() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    this.data.content = [];
    pageNo = 1;
    this.getIndexList();

    setTimeout(function() {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1000);
  },
	
  onReachBottom: function() {
    var that = this;
    ++pageNo;
    this.getIndexList();
  },

  onShareAppMessage: function(res) {
    return {
      title: '空城丶Blog',
      path: '/pages/index/index'
    }
  },

  details(e) {
    //详情页跳转
    wx.navigateTo({
      url: '../details/details?id=' + e.currentTarget.id
    })
  },

  getCategories: function() {
    //获取分类
    var that = this;
    wx.request({
      url: util.basePath + '/article/v1/categories',
      method: "post",
      data: {},
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            navlist: res.data.payload
          });
        }
      }
    });
  },
  getIndexList: function() {
    var that = this;
    that.setData({
      loading: true,
      tip: "正在加载"
    });
    //获取文章列表
    var that = this;
    wx.request({
      url: util.basePath + '/article/v1/getArticleList',
      method: "post",
      data: {
        page: pageNo,
        size: 10
      },
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.payload.status == 200) {
          that.setData({
            // 分页追加数据
            content: that.data.content.concat(res.data.payload.article)
          });
        } else {
					that.setData({
						loading: false,
						tip: "没有数据了"
					});
        }
      }
    });
  }
});