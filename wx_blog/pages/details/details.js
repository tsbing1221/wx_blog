const util = require('../../utils/util.js');
const app = getApp();
const {
  $Toast
} = require('../../dist/base/index');
var id = 1;
Page({
  data: {
		star_img: '/images/star.png',
		star_count:45,
		comment_count: 23,
    userInfo: {},
    article_id: 0,
    isShowModal: true,
    article_comment: '',
		checkStatus: true
  },

  onLoad: function(options) {
    var that = this;
    id = options.id;
    that.setData({article_id: id});
		var checkStatus = wx.getStorageSync("checkStatus");
		this.setData({ checkStatus: checkStatus });
    that.checkUserStar(id);
		that.checkUserRead(id);
		that.getDetails(id);
  },

	__bind_tap: function(e) {
		var that = this;
		var res = e.currentTarget.dataset._el.attr.src;

		if(res) {
			var file_type = res.split('.')[res.split('.').length - 1];

			if(file_type != 'avi' && file_type !='mp4' && file_type != 'mov') {
				var list = this.data.previewImgList || [];
				
				if (list.indexOf(res) == -1) {
					list.push(res);
				}

				wx.previewImage({
					current: res,
					urls: list
				});
			}
		}
	},

	add_user: function() {
		if (!this.data.userInfo) {
			wx.reLaunch({
				url: '/pages/userinfo/userinfo',
			})
		} else {
			wx.setStorage({
				key: 'authorInfo',
				data: { author_openid: this.data.openid, author_name: this.data.author, author_avatar: this.data.avatar },
			});
			
			wx.navigateTo({
				url: '/pages/mypage/mypage?id=' + this.data.article_id
			});
		}
	},

  checkUserStar: function(id) {
    var that = this;

    var userInfo = wx.getStorageSync("loginInfo");
    this.setData({userInfo: userInfo});
    
    if(userInfo) {
			wx.request({
				url: util.basePath + '/article/v1/checkUserStar',
				method: "post",
				data: {
					account: userInfo.account,
					app_sid: userInfo.app_sid,
					article_id: id
				},
				success: res => {
					if (res.data.status == 200) {
						if (res.data.payload.code == 0) {
							that.setData({ star_count: res.data.payload.star_count });
						} else {
							that.setData({ star_count: res.data.payload.star_count, star_img: '/images/star-selected.png' });
						}
						res.data.payload.code == 0 ? '' : that.data.star_img = '/images/star-selected.png';
					} else {
						$Toast({
							content: '请求错误',
							type: 'error'
						});
					}
				}
			});
    } 
  },
	checkUserRead: function (id) {
		var that = this;

		var userInfo = wx.getStorageSync("loginInfo");

		if (userInfo) {
			wx.request({
				url: util.basePath + '/article/v1/checkUserRead',
				method: "post",
				data: {
					account: userInfo.account,
					app_sid: userInfo.app_sid,
					article_id: id
				},
				success: res => {
					if (res.data.status == 200) {
						console.log('阅读成功!');

					}  else {
						console.log(res.data.err);
					}
				}
			});
		} 
	},
  onShareAppMessage: function() {
    return {
      title: '文章详情',
      path: '/pages/details/details?id=' + id
    }
  },
  wxmlTagATap(e) {
    console.log(e);
  },
  onPullDownRefresh() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    this.onLoad();

    setTimeout(function() {
      // 隐藏导航栏加载框
      wx.hideNavigationBarLoading();
      //停止当前页面下拉刷新。
      wx.stopPullDownRefresh()
    }, 1000);
  },

  getDetails: function(id) {
		var that = this;

    wx.request({
      url: util.basePath + '/article/v1/articleDetail/' + id,
      method: "post",
      success: res => {
        if (res.data.status == 200) {
          wx.stopPullDownRefresh();

					//将markdown内容转换为towxml数据
					let data = app.towxml.toJson(res.data.payload.content, 'markdown');
					//设置文档显示主题，默认'light'
					data.theme = 'dark';

          this.setData({
            title: res.data.payload.title,
            labels: res.data.payload.labels,
            gist: res.data.payload.gist,
            created_date: res.data.payload.created_date,
            content: data,
            author: res.data.payload.author,
            read_count: res.data.payload.read_count,
            starsCount: res.data.payload.starsCount,
            CommentCount: res.data.payload.CommentCount,
						avatar: res.data.payload.avatar,
						openid: res.data.payload.openid
          });

        } else {
          $Toast({
            content: '请求错误',
            type: 'error'
          });
        }
      }
    });
  },
  addStar: function() {
    var that = this;

		if(!that.data.userInfo) {
			$Toast({
				content: '请先登录',
				type: 'error'
			});
		} else {
			wx.request({
				url: util.basePath + '/article/v1/addArticleStar',
				method: "post",
				data: {
					article_id: that.data.article_id,
					account: that.data.userInfo.account,
					app_sid: that.data.userInfo.app_sid,
					openid: that.data.openid
				},
				success: res => {
					if (res.data.status == 200) {
						that.setData({ star_img: '/images/star-selected.png' });
						$Toast({
							content: '点赞成功!',
							type: 'success'
						});
					} else {
						that.setData({ star_img: '/images/star.png' });
						$Toast({
							content: '取消点赞成功',
							type: 'success'
						});
					}

					that.checkUserStar(that.data.article_id);
				}
			});
		}
  },
  addComment: function () {
    var that = this;

		wx.setStorage({
			key: 'authorInfo',
			data: { author_openid: this.data.openid, author_name: this.data.author, author_avatar: this.data.avatar },
		});

		if (!that.data.userInfo) {
			$Toast({
				content: '请先登录',
				type: 'error'
			});
		} else {
			wx.navigateTo({
				url: '/pages/comment/comment?id=' + that.data.article_id,
			});
		}
  }
});