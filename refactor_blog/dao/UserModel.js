var Sequelize = require('sequelize');
var sequelize = require('../config').Sequelize;
exports.sequelize = sequelize;
// const Op = Sequelize.Op;

//用户表
var users = sequelize.define('users', {
    uuid: Sequelize.STRING, //用户uuid
    account: Sequelize.STRING, //用户账号
    password: Sequelize.STRING, //用户密码
    app_sid: Sequelize.STRING, //平台标识
    username: Sequelize.STRING, //用户名
    comment: Sequelize.STRING, //备注
    recommend_code: Sequelize.STRING, //用户推荐码
    avatar: Sequelize.STRING, //用户头像url
    created_date: Sequelize.DATE, //用户注册时间
    status: Sequelize.TINYINT, //用户账号状态
    age: Sequelize.TINYINT, //用户年龄
    avatar: Sequelize.STRING, //用户头像
    city: Sequelize.STRING, //用户所在城市
    province: Sequelize.STRING, //用户所在省份
    gender: Sequelize.STRING, //用户性别
    openid: Sequelize.STRING, //用户微信openid
}, {
    freezeTableName: true
});

//积分表
var user_agent = sequelize.define('user_agent', {
    uuid: Sequelize.STRING, //用户uuid
    app_sid: Sequelize.STRING, //平台标识
    account: Sequelize.STRING, //用户账号
    status: Sequelize.STRING, //账号状态
    score: Sequelize.TINYINT, //账号积分
    created_date: Sequelize.DATE //用户注册时间
}, {
    freezeTableName: true
});

//验证码表
var verify_codes = sequelize.define('verify_codes', {
    phone: Sequelize.STRING, //用户账号
    verify_code: Sequelize.STRING, //证件号码
    created_date: Sequelize.DATE, //验证码申请时间
    create_num: Sequelize.TINYINT, //今日创建验证码次数
    err_num: Sequelize.TINYINT, //验证码输入错误次数
    type: Sequelize.TINYINT, //验证码类型
    ip: Sequelize.STRING, //用户真实ip
    app_sid: Sequelize.STRING, //平台标识
}, {
    freezeTableName: true
});

//判断验证码是否存在


//判断账号是否存在
exports.isUserExists = function (account, password, app_sid) {
    return users.findOne({
        where: {
            account: account,
            app_sid: app_sid,
            password: password
        }
    });
}
 
// //修改用户资料
// exports.UpdateUsers = function (user_id, data) {      
//     return users.update(data, {
//         where: {
//             user_id: user_id
//         }
//     });
// }

//注册积分表
exports.createAgent = function (data) {
    return user_agent.create(data);
}

//注册用户表
exports.createUsers = function (data) {
    return users.create(data);
}