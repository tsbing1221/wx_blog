var UserDao = require('../dao/UserModel');
var UserToken = require('../db/user_tokens.js');
var common = require('../util/common');
var config = require('./../config');

module.exports = {
    async regist(data, callback) {
        //验证验证码是否正确
        var userInfo = JSON.parse(JSON.stringify(await UserDao.isCodeExists(data.account, data.app_sid, data.type)));

    },

    async login(data, callback) {
        (async () => {
            //判断账号是否存在
            var userInfo = JSON.parse(JSON.stringify(await UserDao.isUserExists(data.account, data.password, data.app_sid)));

            if (!userInfo) {
                return callback('用户不存在!');
            }

            if (userInfo.user_status != 1) {
                return callback('账号已被禁用!');
            }

            data.app_sid == 'user' ? userInfo.isAdmin = 0 : userInfo.isAdmin = 1;

            //获取登录token
            UserToken.onLogin(userInfo, function (err, data) {
                if (err) {
                    return callback(err);
                }

                userInfo.token = data;
                return callback(null, userInfo);
            });
        })();
    }
};