var DBFactory = require('../db/mysql');
var async = require('async');
// var PostHelper = require('./../utils/postHelper');
var chatDao = module.exports;

chatDao.getOnlineChat = function (data, cb) {
    var sql = 'select id, friendphone, friendname, friendavatar, app_sid, DATE_FORMAT(created_date, "%Y-%m-%d %H:%i:%s") as created_date, userphone, username, useravatar, content, chat_type from online_chat ' +
        'where (friendphone = ? and userphone = ?) or (friendphone = ? and userphone = ?)';
    var value = [data.chatInfo.friendInfo.account, data.chatInfo.userInfo.account, data.chatInfo.userInfo.account, data.chatInfo.friendInfo.account];

    DBFactory.executeSql(sql, value, 'ArticleDao.getOnlineChat', autoCbErrFunction(cb, function (result) {
        var del_info = result && result.length > 0 ? result : null;

        if (!del_info) {
            return cb(null, []);
        }

        return cb(null, del_info);
    }));
}

chatDao.getOnlineChat = function (data, cb) {
    DBFactory.getConnection(function (error, connection) {
        if (error) {
            return cb(error);
        }

        async.waterfall([
            function (callback) {
                connection.beginTransaction(function (err) {
                    return callback(err);
                });
            },
            //通过friendphone查询好友信息
            function (callback) {
                var sql = 'select username, avatar from users where account = ? and app_sid = ?';
                var value = [data.friendphone, data.app_sid];

                connection.query(sql, value, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    if (!result[0]) {
                        return callback('用户不存在!');
                    }
                    data.friendname = result[0].username;
                    data.friendavatar = result[0].avatar;

                    return callback(null, 200);
                });
            },
            //通过userphone查询好友信息
            function (info, callback) {
                var sql = 'select username, avatar from users where account = ? and app_sid = ?';
                var value = [data.userphone, data.app_sid];

                connection.query(sql, value, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    if (!result[0]) {
                        return callback('用户不存在!');
                    }
                    data.username = result[0].username;
                    data.useravatar = result[0].avatar;

                    return callback(null, 200);
                });
            },
            function (release_info, callback) {
                var sql = 'select id, friendphone, friendname, friendavatar, app_sid, DATE_FORMAT(created_date, "%Y-%m-%d %H:%i:%s") as created_date, userphone, username, useravatar, content, chat_type from online_chat ' +
                    'where (friendphone = ? and userphone = ?) or (friendphone = ? and userphone = ?)';
                var value = [data.friendphone, data.userphone, data.userphone, data.friendphone];

                connection.query(sql, value, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    var del_info = result && result.length > 0 ? result : null;

                    if (!del_info) {
                        return callback(null, true, []);
                    }

                    return callback(null, true, del_info);
                });
            }
        ], function (DbErr, isSuccess, uidOrInfo) {
            if (DbErr || !isSuccess) {
                connection.rollback(function () {
                    connection.release();
                });

                return cb(DbErr);
            }

            connection.commit(function (e) {
                if (e) {
                    connection.rollback(function () {
                        connection.release();
                    });

                    return cb(e);
                }

                connection.release();
                cb(null, uidOrInfo);
            });
        });
    });
}

chatDao.saveOnlineChat = function (data, cb) {
    DBFactory.getConnection(function (error, connection) {
        if (error) {
            return cb(error);
        }

        async.waterfall([
            function (callback) {
                connection.beginTransaction(function (err) {
                    return callback(err);
                });
            },
            function (callback) {
                var sql = 'insert into online_chat set ?';
                var value = {
                    friendphone: data.friendInfo.account,
                    friendname: data.friendInfo.username,
                    friendavatar: data.friendInfo.avatar,
                    app_sid: data.friendInfo.app_sid,
                    userphone: data.userInfo.account,
                    username: data.userInfo.username,
                    useravatar: data.userInfo.avatar,
                    created_date: new Date(),
                    status: 1,
                    content: data.chat_content,
                    chat_type: data.chat_type
                };

                connection.query(sql, value, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    if (result.affectedRows == 0) {
                        return callback('聊天出现故障!');
                    }

                    return callback(null, '保存聊天记录成功!');
                });
            },
            function (release_info, callback) {
                var sql = 'select id, friendphone, friendname, friendavatar, app_sid, DATE_FORMAT(created_date, "%Y-%m-%d %H:%i:%s") as created_date, userphone, username, useravatar, content, chat_type from online_chat ' +
                    'where (friendphone = ? and userphone = ?) or (friendphone = ? and userphone = ?)';
                var value = [data.friendInfo.account, data.userInfo.account, data.userInfo.account, data.friendInfo.account];

                connection.query(sql, value, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    var del_info = result && result.length > 0 ? result : null;

                    if (!del_info) {
                        return callback(null, true, []);
                    }

                    return callback(null, true, del_info);
                });
            }
        ], function (DbErr, isSuccess, uidOrInfo) {
            if (DbErr || !isSuccess) {
                connection.rollback(function () {
                    connection.release();
                });

                return cb(DbErr);
            }

            connection.commit(function (e) {
                if (e) {
                    connection.rollback(function () {
                        connection.release();
                    });

                    return cb(e);
                }

                connection.release();
                cb(null, uidOrInfo);
            });
        });
    });
}