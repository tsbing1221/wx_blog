var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
var chat = require('./routes/chat');
 
app.use('/chat/v1', chat);

app.listen(3001);