var syncTask = module.exports;
var backTaskDao = require('./dao/background_task');

function get_news(){
    backTaskDao.get_news({ }, function (err, data) {
        if (err) {
            console.log(err);
        } else {
            if (data.status == 200) {
                console.log(data);
            } else {
                console.log(data);
            }
        }
    }); 
}

function runOnce () {
    syncTask.jobTimerId = 0;

    get_news();
    
    syncTask.jobTimerId = setTimeout(runOnce, 1000 * 60 * 15);//15分钟进程执行一次
}

syncTask.start = function () {
    syncTask.isRunning = true;
    if(syncTask.jobTimerId) return;
    syncTask.goodsIdFinishProcess = 0;
    runOnce();
}
    
syncTask.stop = function () {
    syncTask.isRunning = false;
    if(!syncTask.jobTimerId) return;
    clearTimeout(syncTask.jobTimerId);
    clearTimeout(runOnce);
    syncTask.jobTimerId = 0;
}
