/*
Navicat MySQL Data Transfer

Source Server         : niyueling
Source Server Version : 50728
Source Host           : 49.235.28.88:3306
Source Database       : niyueling

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2020-05-13 11:55:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for add_user_apply
-- ----------------------------
DROP TABLE IF EXISTS `add_user_apply`;
CREATE TABLE `add_user_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myphone` varchar(11) NOT NULL,
  `friendphone` varchar(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `app_sid` varchar(10) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `gist` text,
  `labels` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `author` varchar(30) NOT NULL,
  `openid` varchar(50) DEFAULT NULL,
  `app_sid` varchar(11) NOT NULL,
  `read_count` int(11) DEFAULT '0' COMMENT '点赞次数',
  `uniquekey` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `main_pic` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `article_content` text,
  `article_img` text,
  `file_type` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23356 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for article_comment
-- ----------------------------
DROP TABLE IF EXISTS `article_comment`;
CREATE TABLE `article_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `account` varchar(11) NOT NULL,
  `app_sid` varchar(12) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `author` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `openid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for article_read
-- ----------------------------
DROP TABLE IF EXISTS `article_read`;
CREATE TABLE `article_read` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `account` varchar(11) NOT NULL,
  `app_sid` varchar(12) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=397 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for article_stars
-- ----------------------------
DROP TABLE IF EXISTS `article_stars`;
CREATE TABLE `article_stars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `account` varchar(11) NOT NULL,
  `app_sid` varchar(12) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `openid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for messageboard
-- ----------------------------
DROP TABLE IF EXISTS `messageboard`;
CREATE TABLE `messageboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_id` int(11) DEFAULT NULL,
  `friendphone` varchar(11) CHARACTER SET utf8mb4 NOT NULL COMMENT '留言者',
  `app_sid` varchar(12) CHARACTER SET utf8mb4 NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `friendavatar` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `friendname` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `userphone` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `useravatar` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for online_chat
-- ----------------------------
DROP TABLE IF EXISTS `online_chat`;
CREATE TABLE `online_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friendphone` varchar(11) NOT NULL,
  `friendname` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `friendavatar` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `app_sid` varchar(12) CHARACTER SET utf8mb4 NOT NULL,
  `userphone` varchar(11) NOT NULL,
  `username` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `useravatar` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `created_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  `content` text CHARACTER SET utf8mb4 NOT NULL,
  `chat_type` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for online_chat_save
-- ----------------------------
DROP TABLE IF EXISTS `online_chat_save`;
CREATE TABLE `online_chat_save` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(11) CHARACTER SET utf8mb4 NOT NULL,
  `app_sid` varchar(12) CHARACTER SET utf8mb4 NOT NULL,
  `openid` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `type` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `send_user` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for score_info
-- ----------------------------
DROP TABLE IF EXISTS `score_info`;
CREATE TABLE `score_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(11) NOT NULL,
  `app_sid` varchar(12) NOT NULL,
  `created_date` datetime NOT NULL,
  `type` int(11) NOT NULL,
  `score` decimal(10,0) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_relationship
-- ----------------------------
DROP TABLE IF EXISTS `user_relationship`;
CREATE TABLE `user_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myphone` varchar(11) NOT NULL COMMENT '我的号码',
  `friendphone` varchar(11) NOT NULL COMMENT '好友号码',
  `special_focus` varchar(255) NOT NULL COMMENT '是否为特别关心',
  `status` tinyint(4) NOT NULL COMMENT '0 删除好友 | 1 好友关系 | 2 黑名单',
  `created_date` datetime NOT NULL,
  `rename` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user_score
-- ----------------------------
DROP TABLE IF EXISTS `user_score`;
CREATE TABLE `user_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(11) NOT NULL,
  `app_sid` varchar(12) NOT NULL,
  `status` int(11) NOT NULL,
  `score` decimal(10,0) NOT NULL,
  `created_date` datetime NOT NULL,
  `uuid` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `app_sid` varchar(12) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `uuid` varchar(30) NOT NULL,
  `recommend_code` varchar(11) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `province` varchar(30) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `openid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for verify_codes
-- ----------------------------
DROP TABLE IF EXISTS `verify_codes`;
CREATE TABLE `verify_codes` (
  `phone` varchar(100) NOT NULL COMMENT '手机号',
  `verify_code` varchar(30) DEFAULT NULL COMMENT '验证码',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `create_num` int(10) DEFAULT NULL COMMENT '生成次数',
  `err_num` int(5) DEFAULT '0' COMMENT '输入错误次数',
  `type` int(3) NOT NULL COMMENT '0 | 注册 ， 1 | 忘记',
  `ip` varchar(32) DEFAULT NULL COMMENT 'ip地址',
  `app_sid` varchar(32) NOT NULL,
  PRIMARY KEY (`phone`,`type`,`app_sid`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='验证码表';
